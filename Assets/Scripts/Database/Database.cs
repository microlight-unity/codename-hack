﻿using System.Collections.Generic;
using UnityEngine;

public class Database {
    //Game Tables
    public Dictionary<int, Person> People { get; set; }
    public Dictionary<int, Company> Companies { get; set; }

    //Game Constants
    public Settings Settings { get; }

    public List<string> MaleNames { get; set; }
    public List<string> FemaleNames { get; set; }
    public List<string> LastNames { get; set; }
    public List<string> Aliases { get; set; }

    public List<CompanyName> CompanyNames { get; set; }
    public List<string> CompanyTypes { get; set; }

    //Singleton
    private static Database _instance = null;
    public static Database Instance {
        get {
            if (_instance == null) {
                _instance = new Database();
            }

            return _instance;
        }
    }
    public void Reset() {
        People.Clear();
        Companies.Clear();
    }

    //Contructor
    private Database() {
        //Game Tables
        People = new Dictionary<int, Person>();
        Companies = new Dictionary<int, Company>();

        //Game Constants
        Settings = new Settings();

        MaleNames = new List<string>();
        FemaleNames = new List<string>();
        LastNames = new List<string>();
        Aliases = new List<string>();

        CompanyNames = new List<CompanyName>();
        CompanyTypes = new List<string>();

        //Import
        ImportNames();
    }

    //////////////////////////////////////////////////
    // Imports
    void ImportNames() {
        //Import regular names
        MaleNames = new List<string>(ImportUtility.ImportTextFile("Names/male_names.txt", false));
        FemaleNames = new List<string>(ImportUtility.ImportTextFile("Names/female_names.txt", false));
        LastNames = new List<string>(ImportUtility.ImportTextFile("Names/last_names.txt", false));        
        Aliases = new List<string>(ImportUtility.ImportTextFile("Names/aliases.txt", false));

        //Import company names and types
        List<string> temp = new List<string>(ImportUtility.ImportTextFile("Names/company_names.txt", false));
        foreach (string x in temp) {
            List<string> temp2 = new List<string>(x.Split(';'));

            if (temp2.Count != 2) {
                MDebug.Warning("Database", "ImportNames", "Invalid company name format!", new object[] { temp2.ToString() });
                continue;
            }

            CompanyNames.Add(new CompanyName(temp2[0], temp2[1]));

            //Add type if new type
            if (!CompanyTypes.Contains(temp2[0])) {
                CompanyTypes.Add(temp2[0]);
            }
        }

        MDebug.Success("Imported names!");
    }

    //////////////////////////////////////////////////
    // Logic
    public int CreatePerson() {
        //ID
        int id = 0;
        do {
            id = Utility.GetID();
        } while (People.ContainsKey(id));

        //Gender
        bool gender = false;
        if (Random.Range(0f, 1f) < Settings.MaleSpawnChance) {
            gender = true;
        }

        //Name
        string name = MaleNames[Random.Range(0, MaleNames.Count)];
        string lastName = LastNames[Random.Range(0, LastNames.Count)];
        if (!gender) {
            name = FemaleNames[Random.Range(0, FemaleNames.Count)];
        }

        //Age
        //TODO: finish user creation

        return id;
    }
}
