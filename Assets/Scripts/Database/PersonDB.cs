﻿public class Person {
    //General
    public string Name { get; }
    public string LastName { get; }
    public bool Gender { get; }
    public int DOB { get; }
    public string Email { get; }
    public int BankAcc { get; }
    public string IP { get; }
    public string Alias { get; }

    //Constructor
    public Person(string name, string lastName, bool genderMale, int dob, string email, string ip, string alias) {
        //General
        Name = name;
        LastName = lastName;
        Gender = genderMale;
        DOB = dob;
        Email = email;
        BankAcc = -1;
        IP = ip;
        Alias = alias;
    }
}