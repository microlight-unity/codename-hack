﻿public class Settings {
    //Game settings
    public float MaleSpawnChance { get; }

    //Constructor
    public Settings() {
        MaleSpawnChance = 0.5f;
    }
}
