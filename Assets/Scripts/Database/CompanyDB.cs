﻿using System.Collections.Generic;

public class Company {
    //General
    public string Name { get; }
    public int DOB { get; }
    public string Type { get; }
    public string Email { get; }
    public int BankAcc { get; }
    public List<string> IP { get; }

    //Constructor
    public Company(string name, int dob, string type, string email) {
        //General
        Name = name;
        DOB = dob;
        Type = type;
        Email = email;
        BankAcc = -1;
        IP = new List<string>();
    }
}

public class CompanyName {
    public string Type { get; set; }
    public string Name { get; set; }

    //Constructor
    public CompanyName(string type, string name) {
        Type = type;
        Name = name;
    }
}