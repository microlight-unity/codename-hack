﻿using System.Collections.Generic;
using UnityEngine;

public class StringUtility {
    //Removes char from string at entered index
    public static string RemoveChar(string tString, int index) {
        if (index < 0 && index >= tString.Length) {          //If out of bounds
            return tString;
        }

        string temp = "";          //Temp variable to store changes

        for (int i = 0; i < tString.Length; i++) {
            if (i != index) {          //If its not character that needs to be removed
                temp += tString[i];          //Add char to temp string
            }
        }

        return temp;          //Return new string
    }

    //Removes chars from string, can specify only at start, at end in string or all
    public static string RemoveChars(string tString, char lookingFor = ' ', bool atStart = true, bool atMiddle = true, bool atEnd = true) {
        if (tString.Length < 1) {          //If string is empty
            return tString;
        }

        //Removes spaces at start if enabled
        if (atStart && tString[0] == lookingFor) {
            return RemoveChars(RemoveChar(tString, 0), lookingFor, atStart, atMiddle, atEnd);          //Runs function again but without first character
        }

        //Removes spaces at end if enabled
        if (atEnd && tString[tString.Length - 1] == lookingFor) {
            return RemoveChars(RemoveChar(tString, tString.Length - 1), lookingFor, atStart, atMiddle, atEnd);          //Runs function again but without last character
        }

        //Removes spaces in string
        if (atMiddle) {
            for (int i = 1; i < tString.Length - 1; i++) {
                if (tString[i] == lookingFor) {          //Checks if character is space
                    tString = RemoveChar(tString, i);          //Removes char
                    i--;          //Goes step backwards cause we deleted char
                }
            }
        }

        return tString;          //Returns string
    }

    //Removes characters if they happen more than once in a row (for example space was set twice)
    public static string RemoveDuplicateChars(string word, char c) {
        if (word.Length < 1) {          //If string is empty
            return word;
        }

        bool lastWasChar = false;          //Flag for checking if last char was one we look for

        for (int i = 0; i < word.Length; i++) {
            if (word[i] == c) {          //Checks if character is one we look for
                if (!lastWasChar) {          //If last wasnt char, set flag to true else it removes the char
                    lastWasChar = true;
                }
                else {
                    word = RemoveChar(word, i);          //Removes char
                    i--;          //Goes step backwards cause we deleted char
                }
            }
            else {          //Else if normal character lower the flag
                lastWasChar = false;
            }
        }

        return word;
    }

    //Replaces character at specified index
    public static string ReplaceChar(string tString, char replaceChar, int index) {
        if (index < 0 && index >= tString.Length) {          //If out of bounds
            return tString;
        }

        string temp = "";          //Temp variable to store changes

        for (int i = 0; i < tString.Length; i++) {
            if (i != index) {          //If its not character that needs to be removed
                temp += tString[i];          //Add char to temp string
            }
            else {
                temp += replaceChar;          //Replaces char
            }
        }

        return temp;
    }

    //Replaces all specified characters with specified character
    public static string ReplaceChars(string tString, char target, char replacer) {
        for (int i = 0; i < tString.Length; i++) {
            if (tString[i] == target) {
                tString = ReplaceChar(tString, replacer, i);
            }
        }

        return tString;
    }

    //Inserts character at specified index (char at that position and after will move for one index up)
    public static string InsertChar(string tString, char insertChar, int index) {
        if (index < 0 && index >= tString.Length) {          //If out of bounds
            return tString;
        }

        string temp = "";          //Temp variable to store changes

        for (int i = 0; i < tString.Length; i++) {
            if (i == index) {          //If at position to insert char
                temp += insertChar;          //Inserts char
            }

            temp += tString[i];          //Add char to temp string
        }

        return temp;
    }

    //Replaces string with some characters
    public static string ReplaceString(string msg, char c) {
        string tempString = "";

        foreach (char x in msg) {
            tempString += c;
        }

        return tempString;
    }

    //Returns random char (can decide which characters will be included in generation)
    public static char RandomChar(bool upperletters, bool lowerletters, bool numbers, bool special) {
        List<char> chars = new List<char>();          //Stores all possible characters

        if (upperletters) {
            chars.Add('A');
            chars.Add('B');
            chars.Add('C');
            chars.Add('D');
            chars.Add('E');
            chars.Add('F');
            chars.Add('G');
            chars.Add('H');
            chars.Add('I');
            chars.Add('J');
            chars.Add('K');
            chars.Add('L');
            chars.Add('M');
            chars.Add('N');
            chars.Add('O');
            chars.Add('P');
            chars.Add('Q');
            chars.Add('R');
            chars.Add('S');
            chars.Add('T');
            chars.Add('U');
            chars.Add('V');
            chars.Add('W');
            chars.Add('X');
            chars.Add('Y');
            chars.Add('Z');
        }

        if (lowerletters) {
            chars.Add('a');
            chars.Add('b');
            chars.Add('c');
            chars.Add('d');
            chars.Add('e');
            chars.Add('f');
            chars.Add('g');
            chars.Add('h');
            chars.Add('i');
            chars.Add('j');
            chars.Add('k');
            chars.Add('l');
            chars.Add('m');
            chars.Add('n');
            chars.Add('o');
            chars.Add('p');
            chars.Add('q');
            chars.Add('r');
            chars.Add('s');
            chars.Add('t');
            chars.Add('u');
            chars.Add('v');
            chars.Add('w');
            chars.Add('x');
            chars.Add('y');
            chars.Add('z');
        }

        if (numbers) {
            chars.Add('0');
            chars.Add('1');
            chars.Add('2');
            chars.Add('3');
            chars.Add('4');
            chars.Add('5');
            chars.Add('6');
            chars.Add('7');
            chars.Add('8');
            chars.Add('9');
        }

        if (special) {
            chars.Add('<');
            chars.Add(';');
            chars.Add(',');
            chars.Add('>');
            chars.Add(':');
            chars.Add('.');
            chars.Add('_');
            chars.Add('-');
            chars.Add('*');
            chars.Add('+');
            chars.Add('~');
            chars.Add('/');
            chars.Add('\\');
            chars.Add('!');
            chars.Add('?');
            chars.Add('#');
            chars.Add('$');
            chars.Add('%');
            chars.Add('&');
        }

        if(chars.Count < 1) {
            return '?';
        }

        return chars[Random.Range(0, chars.Count)];          //Returns character from list
    }

    //Returns random string with variables
    public static string RandomString(int chars, bool upperletters = true, bool lowerletters = true, bool numbers = true, bool special = true) {
        //Create string
        string word = "";          //Will store word

        for (int i = 0; i < chars; i++) {
            word += RandomChar(upperletters, lowerletters, numbers, special);
        }

        return word;
    }

    //Returns string with x amount of chars, excess removed and if missing it adds spaces
    public static string StringXLength(string word, int chars, bool fillOnRight = true, bool cutOnRight = true, char fillChar = ' ') {
        while (word.Length > chars)          //If length is longer than amount, cut it
        {
            //Will be character removed from end or start of string (determined by cutOnRight)
            if (cutOnRight) {
                word = RemoveChar(word, word.Length - 1);
            }
            else {
                word = RemoveChar(word, 0);
            }
        }

        while (word.Length < chars)          //If length is lower than amount then add spaces
        {
            //Will be character added at end or start of string (determined by fillOnRight)
            if (fillOnRight) {
                word += fillChar;
            }
            else {
                word = fillChar + word;
            }
        }

        return word;          //Return the string
    }

    //Returns string from List of strings to be for text files
    public static string ListToText(List<string> lines, char delimiter = 'ž') {
        //If list is empty
        if (lines.Count <= 0) {
            return "";
        }

        //If only 1 line inside
        if (lines.Count == 1) {
            return lines[0];
        }

        //Else add eachrow
        string text = lines[0] + delimiter;
        for (int i = 1; i < lines.Count; i++) {
            text += lines[i] + delimiter;
        }

        return RemoveChar(text, text.Length - 1);
    }

    //Trims string at start and end
    public static string TrimString(string word, int fromStart, int fromEnd) {
        if (word.Length <= (fromStart + fromEnd)) {
            return word;
        }

        //Remove characters from front
        while (fromStart > 0) {
            word = RemoveChar(word, 0);
            fromStart--;
        }

        //Remove characters from end
        while (fromEnd > 0) {
            word = RemoveChar(word, word.Length - 1);
            fromEnd--;
        }

        return word;
    }

    //Checks if string contains pattern
    public static bool StringContainPattern(string line, string pattern, bool wholeWord = false, bool caseSensitive = false, bool fromStart = false) {
        //wholeWord determines are we looking for entire word or pattern inside
        //if true and looking for marko, marko returns true while markodva does not return true
        //if false, above example both return true
        //From start means that pattern must be from start if pattern is mark xmark wont work but markx will

        //If empty return true
        if (pattern == "") {
            return true;
        }

        //If pattern longer than line, return false
        if (pattern.Length > line.Length || line == "") {
            return false;
        }

        //Check if contains
        if (wholeWord) {
            //Checking if whole word must match
            foreach (string x in line.Split(' ')) {
                if (caseSensitive && pattern == x) {
                    return true;
                }
                else if (!caseSensitive && pattern.ToUpper() == x.ToUpper()) {
                    return true;
                }
            }
        }
        else {
            int matches = 0;          //How many consecutive matches there are
            for (int i = 0; i < line.Length; i++) {
                //Are we looking at case sensitive data or not
                if (caseSensitive) {
                    //If found matching, increase match count
                    if (line[i] == pattern[matches]) {
                        matches++;
                    }
                    else {
                        //Else set matches to 0 and check again if this character is same as first of pattern
                        matches = 0;

                        if (line[i] == pattern[matches]) {
                            matches++;
                        }
                    }
                }
                else {
                    //If found matching, increase match count
                    if (line[i].ToString().ToUpper() == pattern[matches].ToString().ToUpper()) {
                        matches++;
                    }
                    else {
                        //Else set matches to 0 and check again if this character is same as first of pattern
                        matches = 0;

                        if (line[i].ToString().ToUpper() == pattern[matches].ToString().ToUpper()) {
                            matches++;
                        }
                    }
                }

                //If we found 1 match, return true
                if (matches >= pattern.Length) {
                    return true;
                }

                //If must be from start
                if (fromStart && matches <= i) {
                    return false;
                }
            }
        }

        return false;
    }

    //Removes items from list that dont contain pattern
    public static List<string> GetPatternLines(List<string> list, string pattern, bool wholeWord = false, bool caseSensitive = false, bool fromStart = false, bool reverse = false, bool onlyOne = false) {
        //Reverse means it removes lines that HAVE pattern, otherwise removes lines that DO NOT HAVE pattern
        //Returns list of strings that have been REMOVED
        //And modifies sent list to have only NON REMOVED strings
        List<string> removed = new List<string>();

        for (int i = 0; i < list.Count; i++) {
            if (!reverse && !StringContainPattern(list[i], pattern, wholeWord, caseSensitive, fromStart)) {
                removed.Add(list[i]);
                list.RemoveAt(i);
                i--;

                if (onlyOne) {
                    break;
                }
            }
            else if (reverse && StringContainPattern(list[i], pattern, wholeWord, caseSensitive, fromStart)) {
                removed.Add(list[i]);
                list.RemoveAt(i);
                i--;

                if (onlyOne) {
                    break;
                }
            }
        }

        return removed;
    }

    //Returns the string that best matches the patern (list of strings), also you can add pattern to first clear the list
    public static string GetBestMatch(List<string> inputWords, bool caseSensitive, string pattern = "") {
        //fromStart controls does it c

        //First create new list so we dont change sent list
        List<string> words = new List<string>(inputWords);

        //First we remove unnecesary words from list
        GetPatternLines(words, pattern, false, caseSensitive);

        //If list is empty just return empty string
        if (words.Count < 1) {
            return "";
        }

        //If list has only 1 word we return it
        if (words.Count == 1) {
            return words[0];
        }

        //If more results are left in list, we need to find best match
        int smallestLength = words[0].Length;
        //Get length of smallest string
        foreach (string x in words) {
            if (x.Length < smallestLength) {
                smallestLength = x.Length;
            }
        }

        //Determine pattern
        pattern = "";          //This will store the pattern
        for (int i = 0; i < smallestLength; i++) {
            //Add char to pattern
            pattern += words[0][i];

            //Check each word, if one doesnt have the character, remove last character in pattern and pattern is complete
            foreach (string x in words) {
                if (caseSensitive) {
                    if (x[i] != words[0][i]) {
                        pattern = RemoveChar(pattern, pattern.Length - 1);
                        i = smallestLength;
                        break;
                    }
                }
                else {
                    char upperLetter = x[i].ToString().ToUpper()[0];
                    char lowerLetter = x[i].ToString().ToLower()[0];

                    if (upperLetter != words[0][i] && lowerLetter != words[0][i]) {
                        pattern = RemoveChar(pattern, pattern.Length - 1);
                        i = smallestLength;
                        break;
                    }
                }
            }
        }

        return pattern;
    }
}