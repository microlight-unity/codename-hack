﻿using UnityEngine;

public class MDebug {
    public static void Log(object comment) {
        Debug.Log(comment);
    }
    public static void Success(object comment) {
        Debug.Log("<color=green>" + comment + "</color>");
    }
    public static void Warning(string className, string methodName, object comment, object[] variables) {
        string message = "Class: " + className + ". Method: " + methodName + ". Comment:" + comment;
        foreach (object x in variables) {
            message += "; var:" + x;
        }
        Debug.LogWarning(message);
    }
    public static void Error(string className, string methodName, object comment, object[] variables) {
        string message = "Class: " + className + ". Method: " + methodName + ". Comment:" + comment;
        foreach (object x in variables) {
            message += "; var:" + x;
        }
        Debug.LogError(message);
    }
}
