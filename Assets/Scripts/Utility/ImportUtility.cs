﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ImportUtility {
    // Imports file and returns list of strings, each string is one line
    // First line can be ignored as header
    // Lines starting with ? also can be ignored as they are considered comments
    // Path needs to be entered relative to StreamingAssets folder
    public static List<string> ImportTextFile(string filePath, bool skipHeader = true, bool skipComments = true, char commentsChar = '?', bool stopAtEmpty = true, string stopReadCommand = "", int maxEmptyLines = 20) {
        //Creates path to file
        string path = Application.streamingAssetsPath;
        if (filePath.Length > 0 && filePath[0] != '/') {
            path += "/";
        }
        path += filePath;

        //Stream reader that reads data from file
        if (!File.Exists(path)) {
            MDebug.Error("Utility", "ImportTextFile", "File doesn't exist!", new object[] { path });
            return new List<string>();
        }
        StreamReader reader = new StreamReader(path);

        //If want to skip first line
        if (skipHeader) {
            reader.ReadLine();
        }

        List<string> contents = new List<string>();          //List that stores contents of the file

        int blankLines = 0;          //Counts how many blank lines are in consecutive order
        //Read the file until blank line is encountered or max number of blank lines or end string is encountered
        while (true) {          //Reads until we encounter blank row
            string line = reader.ReadLine();          //Reads new line from reader

            //Blank line counter
            if (line == null || line == "") {
                blankLines++;
            }
            else {
                blankLines = 0;
            }

            //If we are stoping at empty line
            if (stopAtEmpty && blankLines > 0) {
                break;
            }
            //If too many empty lines
            else if (blankLines > maxEmptyLines) {
                MDebug.Warning("Utility", "ImportTextFile", "Exited cause too many empty lines!", new object[] { filePath });
                break;
            }
            //If we ignore empty lines
            else if (!stopAtEmpty && blankLines > 0) {
                continue;
            }
            //If we encountered stop line
            else if (stopReadCommand != "" && line == stopReadCommand) {
                break;
            }

            //If first character is NOT ? (comment char) it adds line to the contents of the file
            else if (!skipComments || (skipComments && line[0] != commentsChar)) {
                contents.Add(line);
            }
        }

        //Stop reader and return contents
        reader.Close();
        return contents;
    }
}