﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour{
    //Objects
    public Database db;

    //References

    //Contructor
    public GameController() {
        db = Database.Instance;
    }

    private void Start() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;
    }
}
