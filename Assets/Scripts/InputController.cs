﻿using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour {
    //Variables
    string inputText;
    string transferText;

    //References
    public InputField cmd;

    private void Start() {
        inputText = transferText = "";   
    }

    private void Update() {
        if (!cmd.isFocused) {
            cmd.ActivateInputField();
        }

        if (Input.GetKeyDown(KeyCode.Return)) {
            MDebug.Success(GetInputText());
        }
    }

    public void OnChange() {
        if(cmd.text != "") {
            MDebug.Log("upisujem: " + cmd.text);
            inputText += cmd.text;
            cmd.text = "";
        }     
    }

    string GetInputText() {
        transferText = inputText;
        inputText = "";
        return transferText;
    }
}
