# Codename Hack

In this simulation of Linux OS your goal is to bypass security of various systems from regular computers to whole banking system.
Steal money from other peoples banking accounts, upgrade your hardware, expose corrupt leaders or help them gain more power, discover various secrets in the world and much more just from your command line.

## About

Game simulates Linux operating system. Made in Unity

* Linux operating system simulation
* Known commands like ls, cd, rm, cat
* Specific commands like connect, disconnect, execute
* Various hacking tools to bypass computer security

## Controls example

![Controls](gitData/Hacker_controls.gif?raw=true "Controls")